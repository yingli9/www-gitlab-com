#!/usr/bin/env ruby

require 'csv'
require 'optparse'
require 'set'
require 'yaml'

require_relative '../lib/bamboohr'

TEAM_YML = 'data/team.yml'.freeze

# Parse command-line options
options = {}
OptionParser.new do |opts|
  opts.on('--csv FILE', 'Write results to a CSV file') do |v|
    options[:csv] = v
  end
  opts.on('-n', '--dry-run', 'Only show what would happen') do |v|
    options[:dryrun] = v
  end
end.parse!

# Load team.yml
raise "File #{TEAM_YML} not found!" unless FileTest.exist? TEAM_YML

team_entries = YAML.load_file(TEAM_YML)

# Load BambooHR data
api_token = ENV['BAMBOOHR_API_TOKEN']
if api_token.nil?
  puts 'Please enter your BambooHR API token:'
  api_token = STDIN.gets.chomp
end
raise 'No BambooHR API token!' if api_token.nil?

client = BambooHR::Client.new(api_token)
bamboo_entries = client.employees

matches, misses = BambooHR::Matcher.match(bamboo_entries, team_entries)

matches.each do |(bamboo_entry, team_entry)|
  team_entry['bamboohr_id'] = bamboo_entry.bamboohr_id
  puts "Match: #{bamboo_entry.name} (#{bamboo_entry.bamboohr_id}) = #{team_entry['name']} (#{team_entry['slug']})"
end
misses.each do |miss|
  puts "Miss: #{miss.name} (#{miss.bamboohr_id})"
end

puts "#{matches.length} Match(es), #{misses.length} Miss(es)"

if options[:csv]
  headers = %w[type bamboohr_id bamboohr_name team_name team_slug]
  CSV.open(options[:csv], 'wb', write_headers: true, headers: headers) do |csv|
    matches.each do |(bamboo_entry, team_entry)|
      csv << ['match', bamboo_entry.bamboohr_id, bamboo_entry.name, team_entry['name'], team_entry['slug']]
    end
    misses.each do |miss|
      csv << ['miss', miss.bamboohr_id, miss.name, '', '']
    end
  end
end

File.write(TEAM_YML, YAML.dump(team_entries)) unless options[:dryrun]
