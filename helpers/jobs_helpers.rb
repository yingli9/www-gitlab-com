module JobsHelpers
  def show_childs(data, item, arr, show_list, depth, dept_id, result, teams)
    item[:child_ids].each do |id|
      res = data.select { |get_item| get_item[:id] == id }

      if arr.include?(res.first[:id]) == false
        if !res.first[:child_ids].empty? || !res.first[:jobs].empty?
          teams << res.first[:id]
          show_teams(res.first, show_list, depth, result)
        end

        if res.first[:jobs].empty? == false && show_list == true
          teams << item[:id]
          teams << dept_id
          show_jobs(res.first, result, depth + 1, teams)
          teams.delete(res.first[:id])
        end
      end

      arr << res.first[:id]
      unless res.first[:child_ids].empty? == true
        show_childs(data, res.first, arr, show_list, depth + 1, dept_id, result, teams)
        teams = []
      end
    end
    return result if result.empty? == false
  end

  def show_teams(data, show_list, depth, result)
    if show_list == true
      result << "<h5 class='team-title' data-team='#{data[:id]}' style='margin-left: #{depth * 10}px'><b>#{data[:name]}</b></h5>".html_safe
    else
      spaces = "&#160;" * depth
      format_name = "#{spaces}#{data[:name]}&#160;"
      result << { name: format_name, id: data[:id] }
    end
  end

  def show_jobs(data, result, depth, teams)
    depth += 1
    values = data[:jobs].each_with_object({}) do |val, res|
      (res[val[:title]] ||= {}).merge!(val) { |key, old, new| old || new }
      res
    end
    return unless values.empty? == false

    values.each do |job|
      result << "<div class='job-container' style='margin-left: #{depth * 10}px' data-teams='#{teams.uniq}' data-depth='#{depth}'><a href='/jobs/apply/#{to_url_param(job.first)}-#{job[1][:id]}'>#{job.first}</a><h6>#{job[1][:location][:name]}</h6></div>".html_safe
    end
  end

  def to_url_param(fragment)
    fragment.downcase.strip.tr(' ', '-').gsub(/[^\w-]/, '')
  end
end
